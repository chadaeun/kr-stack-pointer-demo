import ConfigParser

config = ConfigParser.SafeConfigParser()

config.add_section('PARSER')
config.set('PARSER', 'parser', 'stackptr')
config.set('PARSER', 'test', 'True')
config.set('PARSER', 'model_path', 'models/parsing/stack_ptr_kr_final_without_polysemous_0912/')
config.set('PARSER', 'model_name', 'network.pt')
config.set('PARSER', 'beam', '1')
config.set('PARSER', 'ordered', 'True')
config.set('PARSER', 'gpu', 'True')

config.add_section('SERVER')
config.set('SERVER', 'host', '0.0.0.0')
config.set('SERVER', 'port', '32282')
config.set('SERVER', 'pythonpath', 'lib')

with open('server_http.cfg', 'w') as f:
    config.write(f)