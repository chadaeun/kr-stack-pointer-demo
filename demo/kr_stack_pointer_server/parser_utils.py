# coding=utf-8
import json
import os
from StringIO import StringIO
import torch

from neuronlp2_kr.io import sejong_data, sejong_stacked_data, utils
from neuronlp2_kr.io import SejongWriter
from neuronlp2_kr.io.reader import SejongTestReader
from neuronlp2_kr.models import StackPtrNetKr


class KrStackPointerParser(object):
    def __init__(self, parser='stackptr', test=True, model_path=None, model_name='network.pt',
                 beam=1, ordered=True, gpu=True):

        self.test = bool(test)
        self.model_path = model_path
        self.model_name = model_name
        self.beam = int(beam)
        self.ordered = bool(ordered)
        self.use_gpu = bool(gpu)

        self.alphabets = self.load_alphabets()
        self.reader = None
        self.writer = None
        self.network = None

        if parser == 'stackptr':
            self.load_network()

    def load_network(self):
        word_alphabet, morph_alphabet, syll_alphabet, morph_tag_alphabet, char_alphabet, pos_alphabet, type_alphabet = self.alphabets
        self.reader = SejongTestReader(word_alphabet, morph_alphabet, syll_alphabet, morph_tag_alphabet, char_alphabet,
                                  pos_alphabet)
        self.writer = SejongWriter(word_alphabet, char_alphabet, pos_alphabet, type_alphabet)
        
        model_name = os.path.join(self.model_path, self.model_name)

        arg_path = model_name + '.arg.json'
        arguments = json.load(open(arg_path, 'r'))
        args, kwargs = arguments['args'], arguments['kwargs']

        network = StackPtrNetKr(*args, **kwargs)
        network.load_state_dict((torch.load(model_name)))

        if self.use_gpu:
            network.cuda()
        else:
            network.cpu()

        network.eval()

        self.network = network

    def load_alphabets(self):
        alphabet_path = os.path.join(self.model_path, 'alphabets/')
        alphabets = sejong_data.create_alphabets(alphabet_path, None, data_paths=[None, None], max_vocabulary_size=50000,
                                     morph_embedd_dict=None, syll_embedd_dict=None)
        return alphabets

    def parse_sentence(self, sentence, with_table_header=False):
        result_sejong = self.parse_sentence2sejong(sentence)
        result_dict = self.result_sejong2dict(result_sejong, sentence)

        if with_table_header:
            result_dict['conll'] = '의존소\t지배소\t의존유형\t어절\n'.decode('utf-8') + ('-' * 32).decode('utf-8') + '\n' + result_dict['conll']

        result = {
            'sentences': [result_dict]
        }
        return json.dumps(result, encoding='utf8') + '\n'

    def parse_sentence2sejong(self, sentence):
        # read sentence to data
        word, morphs, sylls, morph_tags, chars, pos, masks, lengths = self.reader.test_data_to_input(sentence, volatile=True,
                                                                                                use_gpu=self.use_gpu)
        heads_pred, types_pred, children_pred, stacked_types_pred = self.network.decode(word, morphs, sylls, morph_tags,
                                                                                   chars,
                                                                                   pos, mask=masks, length=lengths,
                                                                                   beam=self.beam, ordered=self.ordered,
                                                                                   leading_symbolic=sejong_stacked_data.NUM_SYMBOLIC_TAGS)

        # get result in sejong format
        raw_words = ['||'.join(raw_word.split('||')[:-1]) if len(raw_word.split('||')) > 1 else raw_word
                     for raw_word in utils.get_mor_result_v2(sentence.strip().decode('utf-8')).keys()]
        raw_words = [raw_words]

        strio = StringIO()
        self.writer.start_strio(strio)
        self.writer.write(word, pos, heads_pred, types_pred, lengths, symbolic_root=True, ignore_word=False,
                     raw_words=raw_words)
        sejong_result = strio.getvalue()
        self.writer.close()

        return sejong_result

    def result_sejong2dict(self, result_sejong, raw_sent):
        raw_sent = raw_sent.decode('utf-8')
        lines = result_sejong.strip().splitlines()

        sentence = dict()
        sentence['conll'] = result_sejong
        sentence['entities'] = []
        sentence['relations'] = []
        sentence['text'] = raw_sent
        sentence['sentence_offsets'] = [0, len(raw_sent)]
        sentence['token_offsets'] = []
        sentence['tokens'] = []
        sentence['frames'] = []

        offset_before = 0

        for line in lines:
            word_idx, head, deprel, raw_word = line.split('\t')
            word_idx = int(word_idx)
            head = int(head)

            find_offset = raw_sent[offset_before:].find(raw_word) + offset_before
            offset = [find_offset, find_offset + len(raw_word)]
            sentence['token_offsets'].append(offset)
            offset_before = offset[1]

            sentence['entities'].append([
                'T%d' % word_idx,
                '',
                [offset]
            ])

            if head != 0:
                sentence['relations'].append([
                    'R%d' % word_idx,
                    deprel,
                    [['Arg1', 'T%d' % head],
                     ['Arg2', 'T%d' % word_idx]]
                ])

        return sentence