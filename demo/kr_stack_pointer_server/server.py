# coding=utf-8
from __future__ import print_function

import logging
import sys
import os
from StringIO import StringIO

from neuronlp2_kr.io.reader import SejongTestReader

sys.path.append(".")
sys.path.append("..")

import argparse
import json

import torch
from neuronlp2_kr.io import get_logger
from neuronlp2_kr.io import sejong_data, sejong_stacked_data, utils
from neuronlp2_kr.io import SejongWriter
from neuronlp2_kr.models import StackPtrNetKr


def main(args):
    os.chdir(args.cwd)

    logger = get_logger("Analyzer")
    logging.disable(logging.CRITICAL)

    model_path = args.model_path
    model_name = args.model_name

    punct_set = None
    punctuation = args.punctuation
    if punctuation is not None:
        punct_set = set(punctuation)
        logger.info("punctuations(%d): %s" % (len(punct_set), ' '.join(punct_set)))

    use_gpu = args.gpu

    parser = args.parser

    if parser == 'stackptr':
        stackptr(model_path, model_name, punct_set, use_gpu, logger, args)
    else:
        raise ValueError('Unknown parser: %s' % parser)


def stackptr(model_path, model_name, punct_set, use_gpu, logger, args):
    alphabets = load_alphabets(model_path, logger)
    word_alphabet, morph_alphabet, syll_alphabet, morph_tag_alphabet, char_alphabet, pos_alphabet, type_alphabet = alphabets

    reader = SejongTestReader(word_alphabet, morph_alphabet, syll_alphabet, morph_tag_alphabet, char_alphabet, pos_alphabet)
    writer = SejongWriter(word_alphabet, char_alphabet, pos_alphabet, type_alphabet)
    network = load_model(model_path, model_name, use_gpu, logger, args)

    while True:
        sentence = sys.stdin.readline()
        result_sejong = parse_sentence(network, sentence, reader, writer, args)
        result_dict = result_sejong2dict(result_sejong, sentence)
        sys.stdout.write(json.dumps(result_dict, encoding='utf8') + '\n')
        sys.stdout.flush()


def load_alphabets(model_path, logger):
    alphabet_path = os.path.join(model_path, 'alphabets/')
    word_alphabet, morph_alphabet, syll_alphabet, morph_tag_alphabet, char_alphabet, pos_alphabet, type_alphabet = \
        sejong_data.create_alphabets(alphabet_path, None, data_paths=[None, None], max_vocabulary_size=50000,
                                     morph_embedd_dict=None, syll_embedd_dict=None)

    num_words = word_alphabet.size()
    num_morphs = morph_alphabet.size()
    num_sylls = syll_alphabet.size()
    num_morph_tags = morph_tag_alphabet.size()
    num_chars = char_alphabet.size()
    num_pos = pos_alphabet.size()
    num_types = type_alphabet.size()

    logger.info("Word Alphabet Size: %d" % num_words)
    logger.info("Morph Alphabet Size: %d" % num_morphs)
    logger.info("Syll Alphabet Size: %d" % num_sylls)
    logger.info("Morph_tag Alphabet Size: %d" % num_morph_tags)
    logger.info("Character Alphabet Size: %d" % num_chars)
    logger.info("POS Alphabet Size: %d" % num_pos)
    logger.info("Type Alphabet Size: %d" % num_types)

    return word_alphabet, morph_alphabet, syll_alphabet, morph_tag_alphabet, char_alphabet, pos_alphabet, type_alphabet


def load_model(model_path, model_name, use_gpu, logger, args):
    model_name = os.path.join(model_path, model_name)

    beam = args.beam
    ordered = args.ordered
    display_inst = args.display

    def load_model_arguments_from_json():
        arguments = json.load(open(arg_path, 'r'))
        return arguments['args'], arguments['kwargs']

    arg_path = model_name + '.arg.json'
    args, kwargs = load_model_arguments_from_json()

    prior_order = kwargs['prior_order']
    logger.info('use gpu: %s, beam: %d, order: %s (%s)' % (use_gpu, beam, prior_order, ordered))

    logger.info('model: %s' % model_name)
    network = StackPtrNetKr(*args, **kwargs)
    network.load_state_dict(torch.load(model_name))

    if use_gpu:
        network.cuda()
    else:
        network.cpu()

    network.eval()

    return network


def parse_sentence(network, sentence, reader, writer, args):
    # read sentence to data
    word, morphs, sylls, morph_tags, chars, pos, masks, lengths = reader.test_data_to_input(sentence, volatile=True, use_gpu=args.gpu)
    heads_pred, types_pred, children_pred, stacked_types_pred = network.decode(word, morphs, sylls, morph_tags, chars,
                                                                               pos, mask=masks, length=lengths,
                                                                               beam=args.beam, ordered=args.ordered,
                                                                               leading_symbolic=sejong_stacked_data.NUM_SYMBOLIC_TAGS)

    # get result in sejong format
    raw_words = [utils.get_mor_result_v2(sentence.strip().decode('utf-8')).keys()]

    strio = StringIO()
    writer.start_strio(strio)
    writer.write(word, pos, heads_pred, types_pred, lengths, symbolic_root=True, ignore_word=False, raw_words=raw_words)
    sejong_result = strio.getvalue()
    writer.close()

    return sejong_result


def result_sejong2dict(result_sejong, raw_sent):
    raw_sent = raw_sent.decode('utf-8')
    lines = result_sejong.strip().splitlines()

    sentence = dict()
    sentence['conll'] = result_sejong
    sentence['entities'] = []
    sentence['relations'] = []
    sentence['text'] = raw_sent
    sentence['sentence_offsets'] = [0, len(raw_sent)]
    sentence['token_offsets'] = []
    sentence['tokens'] = []
    sentence['frames'] = []

    offset_before = 0

    for line in lines:
        word_idx, head, deprel, raw_word = line.split('\t')
        word_idx = int(word_idx)
        head = int(head)

        find_offset = raw_sent[offset_before:].find(raw_word) + offset_before
        offset = [find_offset, find_offset + len(raw_word)]
        sentence['token_offsets'].append(offset)
        offset_before = offset[1]

        sentence['entities'].append([
            'T%d' % word_idx,
            '',
            [offset]
        ])

        if head != 0:
            sentence['relations'].append([
                'R%d' % word_idx,
                deprel,
                [['Arg1', 'T%d' % head],
                 ['Arg2', 'T%d' % word_idx]]
            ])

    return sentence


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Tuning with stack pointer parser')
    args_parser.add_argument('--parser', choices=['stackptr'], help='Parser', default='stackptr')
    args_parser.add_argument('--test')  # "data/POS-penn/wsj/split1/wsj1.test.original"
    args_parser.add_argument('--model_path', help='path for saving model file.', required=True)
    args_parser.add_argument('--model_name', help='name for saving model file.', default='network.pt')
    args_parser.add_argument('--punctuation', nargs='+', type=str, help='List of punctuations',
                             default=['.', '``', "''", ':', ','])
    args_parser.add_argument('--beam', type=int, default=1, help='Beam size for decoding')
    args_parser.add_argument('--ordered', action='store_true', help='Using order constraints in decoding')
    args_parser.add_argument('--decode', choices=['mst', 'greedy'], default='mst', help='decoding algorithm')
    args_parser.add_argument('--display', action='store_true', help='Display wrong examples')
    args_parser.add_argument('--gpu', action='store_true', help='Using GPU')
    args_parser.add_argument('--cwd', type=str, help='Current Working Directory (should be server home)',
                             default='/home/chadaeun/PycharmProjects/NeuroNLP2/kr_stack_pointer_server')

    args = args_parser.parse_args()
    main(args)