from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from urlparse import urlparse, parse_qs


class ParserHTTPServer(HTTPServer, object):
    def __init__(self, server_address, handler_class, parser):
        super(ParserHTTPServer, self).__init__(server_address, handler_class)
        self._parser = parser

class ParserHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if not isinstance(self.server, ParserHTTPServer):
            self.send_debug('ERROR: ParserHTTPRequestHandler can be used with HTTPServer only.')

        request_arguments = parse_qs(urlparse(self.path).query)

        if 'sentence' not in request_arguments:
            self.send_debug('ERROR: sentence not in request_arguments')
            return

        sentence = request_arguments['sentence'][0].strip().splitlines()[0].strip()

        try:
            result = self.server._parser.parse_sentence(sentence, with_table_header=True)
        except Exception as e:
            self.send_debug(e.message)
            return

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        self.wfile.write(result)
        self.wfile.flush()

    def send_debug(self, message):
        self.send_response(200)
        self.send_header('Content-type', 'text/text')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        self.wfile.write(message)