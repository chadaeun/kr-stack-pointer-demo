import ConfigParser
import sys
sys.path.append('lib')

from server_utils import ParserHTTPServer, ParserHTTPRequestHandler
from parser_utils import KrStackPointerParser


CONFIG_PATH = 'server_http.cfg' # TODO write config file: PARSER section for parser, SERVER section for server

def run(server_class=ParserHTTPServer,
        handler_class=ParserHTTPRequestHandler):
    config = ConfigParser.SafeConfigParser()
    config.read(CONFIG_PATH)

    parser_config = dict(config.items('PARSER'))
    parser = KrStackPointerParser(**parser_config)

    server_address = (config.get('SERVER', 'host'), int(config.get('SERVER', 'port')))
    httpd = server_class(server_address, handler_class, parser)
    httpd.serve_forever()

if __name__ == '__main__':
    run()