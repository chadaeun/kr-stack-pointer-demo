#!/usr/bin/env python

import cgi
import json
from subprocess import Popen, PIPE
import sys
import os

# Parser settings
KR_STACK_POINTER_HOME = '../../parser_server'
KR_STACK_POINTER_LIB = ['/home/chadaeun/PycharmProjects/kr_stack_pointer_git/lib', '/home/chadaeun/PycharmProjects/NeuroNLP2', '/home/chadaeun/PycharmProjects/NeuroNLP2/venv/lib']
KR_STACK_POINTER_MODEL_PATH = '/home/chadaeun/PycharmProjects/NeuroNLP2/models/parsing/stack_ptr_kr_final_without_polysemous_0912/'

for lib in KR_STACK_POINTER_LIB:
    if lib not in os.environ.get('PYTHONPATH', ''):
        os.environ['PYTHONPATH'] = os.environ.get('PYTHONPATH', '') + ':' + lib

# start up parser
parser = Popen(['/home/chadaeun/PycharmProjects/NeuroNLP2/venv/bin/python',
              '%s/server.py' % KR_STACK_POINTER_HOME,
              '--model_path=%s' % KR_STACK_POINTER_MODEL_PATH,
              '--cwd=%s' % KR_STACK_POINTER_HOME],
             stdin=PIPE,
             stdout=PIPE,
               stderr=PIPE)


def get_dep_parses(sentences):
    """ Gets dependency parses as conll from a list of sentences. """
    sys.stderr.write("running Korean Stack Pointer Network...\n")

    results = []
    for sentence in sentences:
        if sentence.strip():
            results.append(_request_one_sentence(sentence))
    sys.stderr.write("parsing done.\n")
    return results
def _request_one_sentence(sentence):
    """
    Gets one dependency parse as conll from a pos tagged conll sentence.
    """
    # one thread at a time, so stdin/out don't get mangled

    parser.stdin.write(sentence.strip() + '\n')
    parser.stdin.flush()
    # line = parser.stderr.readline()
    # print line  # Debugging
    # while line.strip():
    #     line = parser.stderr.readline()
    #     print line  # Debugging
    line = parser.stdout.readline()
    while not line.strip():
        line = parser.stdout.readline()
    result_str = line.strip().decode('utf8')
    result_json = json.loads(result_str)

    return result_json

arguments = cgi.FieldStorage()
sentence = arguments['sentence'].value
sentences = sentence.split('\n')

result = {
    'sentences': get_dep_parses(sentences)
}

print 'Content-Type: application/json'
print
print json.dumps(result)