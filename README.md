# Stack-Pointer Network를 이용한 한국어 의존 구문 분석

2018년 국어 정보 처리 경진대회 지정분야에서 동상을 수상한 한국어 의존 구문 분석 시스템입니다.

2018년 한글 및 한국어 정보처리 학술대회의 'Stack-Pointer Network를 이용한 한국어 의존 구문 분석' *(차다은, 이동엽, 임희석, 2018)* 에서 제안된 시스템입니다.

['Stack-Pointer Networks for Dependency Parsing'](https://arxiv.org/pdf/1805.01087.pdf) *(Xuezhe Ma, Zecong Hu, Jingzhou Liu, Nanyun Peng, Graham Neubig and Eduard Hovy, 2018)* 를 기반으로, Encoder의 입력 단어 표상에서의 자질 보강을 통해 Stack-Pointer Network를 한국어 의존 분석에 적합하도록 확장하였습니다.


- [NeuroNLP2](https://github.com/XuezheMax/NeuroNLP2)의 [Stack-Pointer Networks for Dependency Parsing](https://arxiv.org/pdf/1805.01087.pdf) 구현 코드를 기반으로 하였습니다.
- demo 페이지는 [TurboParser](http://www.ark.cs.cmu.edu/TurboParser/)와 [SEMAFOR](http://www.ark.cs.cmu.edu/SEMAFOR/)의 오픈 소스 웹 demo인 [SemViz](https://github.com/sammthomson/semviz)를 이용하여 구축되었으며, 의존 트리 시각화에는 [brat](http://brat.nlplab.org/)이 사용되었습니다.

---

- `data/`
    - `kmat/`: KMAT 형태소 분석기 디렉터리
- `demo/`: 웹 demo 디렉터리
- `kr_stack_pointer/`: 한국어 대상 Stack-Pointer Network 디렉터리
- `lib/`: 라이브러리 디렉터리

Python 2.7에서 테스트 되었습니다. 실행 전 아래와 같이 필요한 패키지들을 설치합니다.

```
pip install -r requirements.txt
```

## kr_stack_pointer

- `dataset/`: 의존 분석 데이터셋 디렉터리

    의존 분석 데이터셋은 세종 의존 분석 데이터셋의 형식을 따라야 하며, cp949 인코딩을 사용합니다.

    ```
    ; 원본 문장
    단어_인덱스 지배소_단어_인덱스  의존유형    형태소_분석_결과_1    형태소_분석_결과_2
    ...
    ```

    이 때 `형태소_분석_결과_1`은 현재 코드에서 무시합니다.

    예시)

    ```
    ; 디자인 세계 넓혀
    1	2	NP	디자인[NNG]	디자인/NNG
    2	3	NP_OBJ	세계[NNG]	세계/NNG
    3	0	VP	넓히[VV]|어[EC]	넓히/VV|어/EC
    ```

- `glove/`: pre-trained embedding 디렉터리

    현재 코드에서는 형태소, 음절-태그 자질의 pre-trianed embedding 적용이 가능합니다.

    아래와 같이 pre-trianed embedding를 표현한 파일을 gzip을 통해 압축하여 사용합니다.

    ```
    의/JKG -0.168108 1.451054 -0.878510 ...
    하/XSV 0.066654 1.147666 -0.402795 ...
    ```

- `models/`: 학습 모델 저장 디렉터리
- `StackPointerParser_kr.py`: 모델 학습 python 파일

    `run_stackPtrParser_kr.sh`를 통해 실행합니다.

- `run_stackPtrParser_kr.sh`: 모델 학습 python 파일 실행

    작성된 hyperparameter들은 경진대회 참여 모델에서 사용한 hyperparameter입니다.

    실행을 위해 다음 내용을 수정합니다.

    - [NeuroNLP2](https://github.com/XuezheMax/NeuroNLP2)를 clone한 뒤, 첫번째 줄 `NEURO_NLP_PATH`를 해당 경로로 바꿈
    - `morph_path`, `syll_path`에 pre-trained embedding 파일 경로 지정
    - `train`, `test`, `dev`에 데이터셋 경로 지정
    - `model_path`에 모델을 저장할 경로 지정

- `analyze_kr.py`: 모델 테스트 python 파일

    `run_analyze_kr.sh`를 통해 실행합니다.

- `run_analyze_kr.sh`: 모델 테스트 python 파일 실행

    실행을 위해 다음 내용을 수정합니다.
    
    - [NeuroNLP2](https://github.com/XuezheMax/NeuroNLP2)를 clone한 뒤, 첫번째 줄 `NEURO_NLP_PATH`를 해당 경로로 바꿈
    - `test`에 테스트 데이터셋 경로 지정
    - `model_path`에 학습한 모델 경로를 지정

## demo

- `demo_server/`: 웹 demo 서버 디렉터리
    - `settings.py`

        실행을 위해 다음 내용을 수정합니다.

        - [NeuroNLP2](https://github.com/XuezheMax/NeuroNLP2)를 clone한 뒤, `NEURO_NLP_PATH`를 해당 경로로 바꿈
        - `MODEL_PATH`를 사용할 모델 경로로 지정
    - `web_app.py`: 웹 demo 서버를 실행합니다.
  
- `parser_server/`: 웹 demo에서 사용하는 parser 코드 디렉터리