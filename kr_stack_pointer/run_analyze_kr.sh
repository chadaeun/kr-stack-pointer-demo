#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0 PYTHONPATH=:../lib:NEURO_NLP_PATH python analyze_kr.py --parser stackptr --test \
 --model_path "models/MODEL_PATH/" --model_name "network.pt" --beam \
 --punctuation '.' '``' "''" ':' ',' --gpu \
 --test "dataset/TEST_DATASET.txt"
